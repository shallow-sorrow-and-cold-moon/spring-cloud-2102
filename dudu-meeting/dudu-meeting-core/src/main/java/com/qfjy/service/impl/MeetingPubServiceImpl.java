package com.qfjy.service.impl;

import com.qfjy.entity.po.MeetingPub;
import com.qfjy.mapper.MeetingPubMapper;
import com.qfjy.service.MeetingPubService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName MeetingPubServiceImpl
 * @Description TODO
 * @Author guoweixin
 * @Date 2021/8/18
 * @Version 1.0
 */
@Slf4j
@Service
public class MeetingPubServiceImpl  implements MeetingPubService {

    @Autowired
    private MeetingPubMapper meetingPubMapper;
    /**
     * 根据会议编号查询会议信息
     *
     * @param pcode
     */
    @Override
    public MeetingPub selectMeetingPubByPcode(String pcode) {
        return meetingPubMapper.selectMeetingPubByPcode(pcode);
    }
}
