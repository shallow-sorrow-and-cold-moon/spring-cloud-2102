package com.qfjy.controller;

import com.qfjy.entity.po.MeetingPub;
import com.qfjy.service.MeetingPubService;
import com.qfjy.util.result.ResultCode;
import com.qfjy.util.result.ResultJson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * @ClassName MeetingPubController
 * @Description TODO
 * @Author guoweixin
 * @Date 2021/8/18
 * @Version 1.0
 */
@Controller
@RequestMapping("meetingPub")
@Slf4j
public class MeetingPubController {

    @Autowired
    private MeetingPubService meetingPubService;
    /**
     * 根据会议编号查询会议详细信息
     */
    @GetMapping("{pcode}")   // meetingPub/pcode
    @ResponseBody
    public Object selectById(@PathVariable("pcode")  String pcode){
         MeetingPub meetingPub= meetingPubService.selectMeetingPubByPcode(pcode);

         if(meetingPub==null){
             return  new ResultJson<>(null, ResultCode.NOT_DATA);
         }

         log.info("会议微服务---》根据会议编号查询会议信息-->"+meetingPub);

        /**
         * 查询视频微服务相关信息
         */

        /**
         * param1 url 请求的URL地址
         * param2 responseType 希望返回的类型.class
         * param3 占位符 最后一个参数是map,map的key为前面的占位符。map value为参数值
         * 可变长度的参数，一一来替换前面的占位符 name={name}形式，
         */
        String url="http://localhost:8085/videoInfo/info?pcode="+meetingPub.getPcode();
         String result=restTemplate.getForObject(url,String.class);

         meetingPub.setRemark(result);

        return new ResultJson<>(meetingPub,ResultCode.SUCCESS);
    }

    @Autowired
    private RestTemplate restTemplate;
}
