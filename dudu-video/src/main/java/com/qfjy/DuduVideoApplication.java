package com.qfjy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DuduVideoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DuduVideoApplication.class, args);
    }

}
