package com.qfjy.service.impl;

import com.qfjy.service.VideoInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @ClassName VideoInfoServiceImpl
 * @Description TODO
 * @Author guoweixin
 * @Date 2021/8/18
 * @Version 1.0
 */
@Service
@Slf4j
public class VideoInfoServiceImpl implements VideoInfoService {
    /**
     * 根据会议编号 查询视频相关召开信息。
     * 分布式ID 编号（会议编号）
     *
     * @param pcode
     */
    @Override
    public String selectVideoInfoByPcode(String pcode) {
        return "该场会议:召开了59分钟，在线人数30人。网络正常 会议编号是："+pcode;
    }
}
