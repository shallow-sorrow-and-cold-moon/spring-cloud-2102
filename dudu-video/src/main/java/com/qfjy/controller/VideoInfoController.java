package com.qfjy.controller;

import com.qfjy.service.VideoInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName VideoInfoController
 * @Description TODO
 * @Author guoweixin
 * @Date 2021/8/18
 * @Version 1.0
 */
@RequestMapping("videoInfo")

@Slf4j
@RestController  // @Controller
public class VideoInfoController {

    @Autowired
    private VideoInfoService videoInfoService;

    @GetMapping("info")    //  videoInfo/info?pcode=11111
    public String selectVideoInfoByPcode(@RequestParam("pcode") String pcode){
        return videoInfoService.selectVideoInfoByPcode(pcode);
    }
}
